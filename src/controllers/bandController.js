'use strict'
import { createItem , searchItems, searchItemById, updateItem , deleteItem, addToItemArray, removeFromItemArray } from '../databases/itemDatabase';

const type = 'band';

/**
 * Post band.
 */
export const httpPostBand = async (request, response) => {
    console.log(`HTTP POST`);

    var band = request.body;

    try {
        const result = await createItem(band, type);

        if ( result !== undefined ) {
            return response.status(201).send(result);
        } else {
            return response.status(400).send('Bad request; no band returned.');
        }
    } catch(error) {
        return response.status(400).send(error);
    }
}

/**
 * Get all bands.
 */
export const httpGetBands = async (request, response) => {
    console.log(`HTTP GET ALL`);

    try {
        const result = await searchItems(type);

        if ( result !== undefined) {
            return response.status(200).send(result);
        } else {
            return response.status(404).send('Bands not found');
        } 
    } catch(error) {
        return response.status(400).send(error);
    }
}

/**
 * Get band.
 */
export const httpGetBand = async (request, response) => {
    console.log(`HTTP GET`);

    var id = request.params.band_id;

    try {
        const result = await searchItemById(id);

        if ( result !== undefined) {
            return response.status(200).send(result);
        } else {
            return response.status(404).send('Band not found');
        }
    } catch(error) {
        return response.status(400).send(error);
    }
}

/**
 * Update band.
 */
export const httpPutBand = async (request, response) => {
    console.log(`HTTP PUT`);

    var id = request.params.band_id;
    var band = request.body;

    try {
        const result = await updateItem(id, band);

        if ( result !== undefined) {
            return response.status(200).send(result);
        } else {
            return response.status(404).send('Band not found');
        }
    } catch (error) {
        return response.status(400).send(error);
    }
}

/**
 * Delete band.
 */
export const httpDeleteBand = async (request, response) => {
    console.log(`HTTP DELETE`);

    const id = request.params.band_id;

    try {
        const result = await deleteItem(id);

        if ( result ) {
            return response.status(204).send(result);
        } else {
            return response.status(404).send('Band not found');
        }
    } catch(error) {
        return response.status(400).send(error);
    }
}

export const httpPostBandMember = async(request, response) => {
    console.log(`HTTP POST BAND MEMBER`);

    const id = request.params.band_id;
    const member = request.body;

    try {
        const result = await addToItemArray(id, 'members', member.id);

        if ( result ) {
            return response.status(200).send(result);
        } else {
            return response.status(404).send('Band not found');
        }
    } catch(error) {
        return response.status(400).send(error);
    }

}

export const httpDeleteBandMember = async(request, response) => {
    console.log(`HTTP DELETE BAND MEMBER`);

    const band_id = request.params.band_id;
    const member_id = request.params.member_id;

    try {
        const result = await removeFromItemArray(band_id, 'members', member_id);
        
        if ( result ) {
            return response.status(200).send(result);
        } else {
            return response.status(404).send('Band not found');
        }
    } catch(error) {
        return response.status(400).send(error);
    }

}