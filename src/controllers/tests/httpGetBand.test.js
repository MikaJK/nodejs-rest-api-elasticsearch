import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const getTestRequest = supertest(app);
const sinon = require("sinon");
const itemDatabase = require('../../databases/itemDatabase');

var sandbox;

var band = {
    "id": "C0tDfW4BfrRsb4l-McNY",
    "name": "Stamina",
    "genre": "Suomi-metalli"
}

describe('GET BAND', () => {

    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it('get one band', async done => {
        
        sandbox.stub(itemDatabase, 'searchItemById').resolves(band);
        
        await getTestRequest.get(`/bands/${band.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.id).toBe(band.id);
                expect(response.body.name).toBe(band.name);
                expect(response.body.genre).toBe(band.genre);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.searchItemById);
    });

    it('get not found error', async done => {
        
        sandbox.stub(itemDatabase, 'searchItemById').resolves(undefined);
        
        await getTestRequest.get(`/bands/${band.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.searchItemById);
    });

    it('get bad request error', async done => {
        
        sandbox.stub(itemDatabase, 'searchItemById').rejects();
        
        await getTestRequest.get(`/bands/${band.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(400);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.searchItemById);
    });
});
