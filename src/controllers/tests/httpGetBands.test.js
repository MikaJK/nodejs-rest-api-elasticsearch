import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const getTestRequest = supertest(app);
const sinon = require("sinon");
const itemDatabase = require('../../databases/itemDatabase');

var sandbox;

var bands = [
    {
        "id": "C0tDfW4BfrRsb4l-McNY",
        "name": "Stamina",
        "genre": "Suomi-metalli"
    },
    {
        "id": "DUtQfW4BfrRsb4l-hMN-",
        "name": "Kotiteollisuus",
        "genre": "Suomi-rock"
    }
]

describe('GET BANDS', () => {

    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it('get two bands', async done => {
        
        sandbox.stub(itemDatabase, 'searchItems').resolves(bands);
        
        await getTestRequest.get(`/bands`)
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.length).toBe(2);
                // expect(response.body.id).toBe(band.id);
                // expect(response.body.name).toBe(band.name);
                // expect(response.body.genre).toBe(band.genre);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.searchItems);
    });

    it('get not found error', async done => {
        
        sandbox.stub(itemDatabase, 'searchItems').resolves(undefined);
        
        await getTestRequest.get(`/bands`)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.searchItems);
    });

    it('get bad request error', async done => {
        
        sandbox.stub(itemDatabase, 'searchItems').rejects();
        
        await getTestRequest.get(`/bands`)
            .then((response) => {
                expect(response.statusCode).toBe(400);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.searchItems);
    });
});
