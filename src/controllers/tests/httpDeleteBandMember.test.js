import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const deleteTestRequest = supertest(app);
const sinon = require("sinon");
const itemDatabase = require('../../databases/itemDatabase');

var sandbox;

var bandWithMember = {
    "id": "C0tDfW4BfrRsb4l-McNY",
    "name": "Stamina",
    "genre": "Suomi-metalli",
    "members": [
        "t0tjrm4B7pbDJT5cK5ST"
    ]
}

var member = {
    "id": "t0tjrm4B7pbDJT5cK5ST",
    "first_name": "Antti",
    "last_name": "Hyyrynen"
}

var bandWithoutMember = {
    "id": "C0tDfW4BfrRsb4l-McNY",
    "name": "Stamina",
    "genre": "Suomi-metalli",
    "members": []
}



describe('DELETE BAND MEMBER', () => {

    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it('remove one band member', async done => {
        
        sandbox.stub(itemDatabase, 'removeFromItemArray').resolves(bandWithoutMember);
        
        await deleteTestRequest.delete(`/bands/${bandWithMember.id}/members/${member.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.id).toBe(bandWithoutMember.id);
                expect(response.body.name).toBe(bandWithoutMember.name);
                expect(response.body.genre).toBe(bandWithoutMember.genre);
                expect(response.body.members.length).toBe(0);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.removeFromItemArray);
    });

    it('get not found error', async done => {
        
        sandbox.stub(itemDatabase, 'removeFromItemArray').resolves(undefined);
        
        await deleteTestRequest.delete(`/bands/${bandWithMember.id}/members/${member.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.removeFromItemArray);
    });

    it('get bad request error', async done => {
        
        sandbox.stub(itemDatabase, 'removeFromItemArray').rejects();
        
        await deleteTestRequest.delete(`/bands/${bandWithMember.id}/members/${member.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(400);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.removeFromItemArray);
    });
});
