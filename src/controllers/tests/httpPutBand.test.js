import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const putTestRequest = supertest(app);
const sinon = require("sinon");
const itemDatabase = require('../../databases/itemDatabase');

var sandbox;

var updatedBand = {
    "id": "C0tDfW4BfrRsb4l-McNY",
    "name": "Stamina",
    "genre": "Suomi-metalli"
}

describe('PUT BAND', () => {

    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it('update one band', async done => {
        
        sandbox.stub(itemDatabase, 'updateItem').resolves(updatedBand);
        
        await putTestRequest.put(`/bands/${updatedBand.id}`).send(updatedBand)
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.id).toBe(updatedBand.id);
                expect(response.body.name).toBe(updatedBand.name);
                expect(response.body.genre).toBe(updatedBand.genre);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.updateItem);
    });

    it('get not found error', async done => {
        
        var unknownId = "D0tDfW4BfrRsb4l-McNY";

        sandbox.stub(itemDatabase, 'updateItem').resolves(undefined);
        
        await putTestRequest.put(`/bands/${unknownId}`).send(updatedBand)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.updateItem);
    });

    it('get bad request error', async done => {
        
        sandbox.stub(itemDatabase, 'updateItem').rejects();
        
        await putTestRequest.put(`/bands/${updatedBand.id}`).send(updatedBand)
            .then((response) => {
                expect(response.statusCode).toBe(400);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.updateItem);
    });
});
