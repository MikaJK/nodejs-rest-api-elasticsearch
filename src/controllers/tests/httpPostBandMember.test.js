import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const postTestRequest = supertest(app);
const sinon = require("sinon");
const itemDatabase = require('../../databases/itemDatabase');

var sandbox;

var bandWithoutMember = {
    "id": "C0tDfW4BfrRsb4l-McNY",
    "name": "Stamina",
    "genre": "Suomi-metalli"
}

var member = {
    "id": "t0tjrm4B7pbDJT5cK5ST",
    "first_name": "Antti",
    "last_name": "Hyyrynen"
}

var bandWithMember = {
    "id": "C0tDfW4BfrRsb4l-McNY",
    "name": "Stamina",
    "genre": "Suomi-metalli",
    "members": [
        "t0tjrm4B7pbDJT5cK5ST"
    ]
}

describe('POST BAND MEMBER', () => {

    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it('add one band member', async done => {
        
        sandbox.stub(itemDatabase, 'addToItemArray').resolves(bandWithMember);
        
        await postTestRequest.post(`/bands/${bandWithoutMember.id}/members`).send(member)
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.id).toBe(bandWithMember.id);
                expect(response.body.name).toBe(bandWithMember.name);
                expect(response.body.genre).toBe(bandWithMember.genre);
                expect(response.body.members[0]).toBe(member.id);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.addToItemArray);
    });

    it('get not found error', async done => {
        
        sandbox.stub(itemDatabase, 'addToItemArray').resolves(undefined);
        
        await postTestRequest.post(`/bands/${bandWithoutMember.id}/members`).send(member)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.addToItemArray);
    });

    it('get bad request error', async done => {
        
        sandbox.stub(itemDatabase, 'addToItemArray').rejects();
        
        await postTestRequest.post(`/bands/${bandWithoutMember.id}/members`).send(member)
            .then((response) => {
                expect(response.statusCode).toBe(400);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.addToItemArray);
    });
});
