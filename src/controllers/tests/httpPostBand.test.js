import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const postTestRequest = supertest(app);
const sinon = require("sinon");
const itemDatabase = require('../../databases/itemDatabase');

var sandbox;

var newBand = {
    "name": "Stamina",
    "genre": "Suomi-metalli"
}

var createdBand = {
    "id": "C0tDfW4BfrRsb4l-McNY",
    "name": "Stamina",
    "genre": "Suomi-metalli"
}



describe('POST BAND', () => {

    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it('create one band', async done => {
        
        sandbox.stub(itemDatabase, 'createItem').resolves(createdBand);
        
        await postTestRequest.post(`/bands`).send(newBand)
            .then((response) => {
                expect(response.statusCode).toBe(201);
                expect(response.body.id).toBe(createdBand.id);
                expect(response.body.name).toBe(createdBand.name);
                expect(response.body.genre).toBe(createdBand.genre);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.createItem);
    });

    it('get bad request error', async done => {
        
        sandbox.stub(itemDatabase, 'createItem').rejects();
        
        await postTestRequest.post(`/bands`).send(newBand)
            .then((response) => {
                expect(response.statusCode).toBe(400);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.createItem);
    });

    it('get bad request error; no band returned', async done => {
        
        sandbox.stub(itemDatabase, 'createItem').resolves(undefined);
        
        await postTestRequest.post(`/bands`).send(newBand)
            .then((response) => {
                expect(response.statusCode).toBe(400);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.createItem);
    });
});
