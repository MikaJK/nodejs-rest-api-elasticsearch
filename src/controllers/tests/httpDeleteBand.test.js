import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const deleteTestRequest = supertest(app);
const sinon = require("sinon");
const itemDatabase = require('../../databases/itemDatabase');

var sandbox;

var deletedBand = {
    "id": "C0tDfW4BfrRsb4l-McNY",
    "name": "Stamina",
    "genre": "Suomi-metalli"
}

describe('DELETE BAND', () => {

    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it('delete one band', async done => {
        
        sandbox.stub(itemDatabase, 'deleteItem').resolves(true);
        
        await deleteTestRequest.delete(`/bands/${deletedBand.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(204);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.deleteItem);
    });

    it('get not found error', async done => {
        
        sandbox.stub(itemDatabase, 'deleteItem').resolves(false);
        
        await deleteTestRequest.delete(`/bands/${deletedBand.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.deleteItem);
    });

    it('get bad request error', async done => {
        
        sandbox.stub(itemDatabase, 'deleteItem').rejects();
        
        await deleteTestRequest.delete(`/bands/${deletedBand.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(400);
                done();
            });

        sandbox.assert.calledOnce(itemDatabase.deleteItem);
    });
});
