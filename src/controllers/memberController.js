'use strict'
import { createItem , searchItems, searchItemById, searchItemByKey, updateItem , deleteItem } from '../databases/itemDatabase';

const type = 'member';

/**
 * Post member.
 */
export const httpPostMember = async (request, response) => {
    console.log(`HTTP POST MEMBER`);

    var member = request.body;

    try {
        const result = await createItem(member, type);

        if ( result !== undefined ) {
            return response.status(201).send(result);
        } else {
            return response.status(400).send('Bad request; no member returned.');
        }
    } catch(error) {
        return response.status(400).send(error);
    }
}

/**
 * Get all members.
 */
export const httpGetMembers = async (request, response) => {
    console.log(`HTTP GET ALL MEMBERS`);

    try {
        const result = await searchItems(type);

        if ( result !== undefined) {
            return response.status(200).send(result);
        } else {
            return response.status(404).send('Members not found');
        } 
    } catch(error) {
        return response.status(400).send(error);
    }
}

/**
 * Get member.
 */
export const httpGetMember = async (request, response) => {
    console.log(`HTTP GET MEMBER`);

    var id = request.params.member_id;
    console.log(id);

    try {
        const result = await searchItemById(id);

        if ( result !== undefined) {
            return response.status(200).send(result);
        } else {
            return response.status(404).send('Member not found');
        }
    } catch(error) {
        return response.status(400).send(error);
    }
}

/**
 * Update member.
 */
export const httpPutMember = async (request, response) => {
    console.log(`HTTP PUT MEMBER`);

    var id = request.params.member_id;
    var member = request.body;

    try {
        const result = await updateItem(id, member);

        if ( result !== undefined) {
            return response.status(200).send(result);
        } else {
            return response.status(404).send('Member not found');
        }
    } catch (error) {
        return response.status(400).send(error);
    }
}

/**
 * Delete member.
 */
export const httpDeleteMember = async (request, response) => {
    console.log(`HTTP DELETE MEMBER`);

    const id = request.params.member_id;

    try {
        const result = await deleteItem(id);

        if ( result ) {
            return response.status(204).send(result);
        } else {
            return response.status(404).send('Member not found');
        }
    } catch(error) {
        return response.status(400).send(error);
    }
}
