import { createItem , searchItems, searchItemById, searchItemByKey, updateItem , deleteItem } from '../databases/itemDatabase';

export class ItemController {

    constructor(type) {
        this.type = type;
        console.log(`TYPE: ${this.type}`);
    }

    /**
     * Post item.
     */
    async httpPostItem(request, response) {
        console.log(`HTTP POST`);

        var item = request.body;

        try {
            //const result = await createItem(item, this.type);
            const result = await createItem(item, this.type);

            if ( result !== undefined ) {
                return response.status(201).send(result);
            } else {
                return response.status(400).send(`Bad request; no ${this.type} object returned.`);
            }
        } catch(error) {
            return response.status(400).send(error);
        }
    }

    /**
     * Get all items.
     */
    async httpGetItems(request, response) {
        console.log(`HTTP GET ALL for`);

        try {
            const result = await searchItems(this.type);
            //const result = await searchItems('band');

            if ( result !== undefined) {
                return response.status(200).send(result);
            } else {
                return response.status(404).send(`No ${this.type} objects found.`);
                //return response.status(404).send(`No band objects found.`);
            } 
        } catch(error) {
            console.log(`ERROR ${error}`);
            return response.status(400).send(error);
        }
    }

    /**
     * Get item.
     */
    async httpGetItem(request, response) {
        console.log(`HTTP GET`);

        var id = request.params.id;

        try {
            const result = await searchItemById(id);

            if ( result !== undefined) {
                return response.status(200).send(result);
            } else {
                return response.status(404).send(`No ${this.type} object found.`);
            }
        } catch(error) {
            return response.status(400).send(error);
        }
    }

    /**
     * Update item.
     */
    async httpPutItem(request, response) {
        console.log(`HTTP PUT`);

        var id = request.params.id;
        var item = request.body;

        try {
            const result = await updateItem(id, item);

            if ( result !== undefined) {
                return response.status(200).send(result);
            } else {
                return response.status(404).send(`No ${this.type} objects found.`);
            }
        } catch (error) {
            return response.status(400).send(error);
        }
    }

    /**
     * Delete item.
     */
    async httpDeleteItem(request, response) {
        console.log(`HTTP DELETE`);

        const id = request.params.id;

        try {
            const result = await deleteItem(id);

            if ( result ) {
                return response.status(204).send(result);
            } else {
                return response.status(404).send(`No ${this.type} objects found.`);
            }
        } catch(error) {
            return response.status(400).send(error);
        }
    }
}
