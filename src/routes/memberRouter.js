'use strict';
var member = require('../controllers/memberController');

export default function memberRouter(app)
{
    app.route('/members')
        .get(member.httpGetMembers)
        .post(member.httpPostMember);


    app.route('/members/:member_id')
        .get(member.httpGetMember)
        .put(member.httpPutMember)
        .delete(member.httpDeleteMember);
};