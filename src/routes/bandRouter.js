'use strict';
var band = require('../controllers/bandController');

export default function bandRouter(app)
{
    app.route('/bands')
        .get(band.httpGetBands)
        .post(band.httpPostBand);


    app.route('/bands/:band_id')
        .get(band.httpGetBand)
        .put(band.httpPutBand)
        .delete(band.httpDeleteBand);

    app.route('/bands/:band_id/members')
        // actually this should be put, becasue existing resource is updated.
        .post(band.httpPostBandMember);

    app.route('/bands/:band_id/members/:member_id')
        // actually this should be put, becasue existing resource is updated.
        .delete(band.httpDeleteBandMember);

    // app.route('/bands/:band_id/tags')
    //     .post(band.postTag);
    //
    // app.route('/Bands/:band_id/tags/:tag')
    //     .delete(band.deleteTag);
};