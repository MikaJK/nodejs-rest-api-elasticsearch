import dotenv from 'dotenv';
import { createDocument, updateDocument, deleteDocument, searchDocument, searchDocumentById, addValueToArray, removeValueFromArray } from '../utils/documentUtil';

dotenv.config();

const indexName = process.env.INDEX_NAME;

/**
 * Create item.
 */
export const createItem = (item, type) => {
    console.log(`CREATE ITEM`);
    
    item.type = type;

    return new Promise((resolve, reject) => {
        createDocument(indexName, item)
            .then(async response => {
                if ( response.result === 'created' ) {
                    var createdItem = await searchItemById(response._id);
                    resolve(createdItem);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Search items.
 */
export const searchItems = (type) => {
    console.log(`SEARCH ITEMS`);
    
    const query = {
        query: {
            bool: {
                must: {
                    match: {
                        type: type
                    }
                }
            }
        }
    }

    var items = new Array();
    
    return new Promise((resolve, reject) => {
        searchDocument(indexName, query)
            .then(response => {
                if ( response.hits.total.value > 0) {
                    response.hits.hits.forEach(hit => {
                        items.push(composeItem(hit));
                    });
                    resolve(items);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Search item by key.
 */
export const searchItemByKey = (key, value, type) => {
    console.log(`SEARCH ITEM BY KEY`);

    const query = {
        query: {
            bool: {
                must: {
                    match: {
                        [key]: value
                    }
                },
                filter: {
                    term: {
                        type: type
                    }
                }
            }
        }
    }

    var item;

    return new Promise((resolve, reject) => {
        searchDocument(indexName, query)
            .then(response => {
                if ( response.hits.total.value > 0) {
                    response.hits.hits.forEach(hit => {
                        if ( hit._score === response.hits.max_score ) {
                            item = composeBand(hit);
                        }
                    });

                    resolve(item);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Search item by ID.
 */
export const searchItemById = (id) => {
    console.log(`SEARCH ITEM BY ID`);

    var item;
    
    return new Promise((resolve, reject) => {
        searchDocumentById(indexName, id)
            .then(response => {
                if ( response.found ) {
                    item = composeItem(response);
                    resolve(item);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Update item.
 */
export const updateItem = (id, item) => {
    console.log(`UPDATE ITEM`);

    var updatedItem;

    return new Promise((resolve, reject) => {
        updateDocument(indexName, id, item)
            .then(async response => {
                if ( response.result === 'updated' || response.result === 'noop' ) {
                    updatedItem = await searchItemById(id);
                    resolve(updatedItem);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Delete item.
 */
export const deleteItem = (id) => {
    console.log(`DELETE ITEM`);

    return new Promise((resolve, reject) => {
        deleteDocument(indexName, id)
            .then(response => {
                if ( response.result === 'deleted' ) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

export const addToItemArray = (id, arrayName, value) => {
    console.log(`ADD ITEM TO ARRAY`);

    var updatedItem;

    return new Promise((resolve, reject) => {
        addValueToArray(indexName, id, arrayName, value)
            .then(async response => {
                if (response.result === 'updated' || response.result === 'noop' ) {
                    updatedItem = await searchItemById(id);
                    console.log(`THEN OK`);
                    resolve(updatedItem);
                } else {
                    console.log(`THEN NOK`);
                    resolve(undefined);
                }
            })
            .catch(error => {
                console.log(`CATCH`);
                reject(error);
            });
    });
}

export const removeFromItemArray = (id, arrayName, value) => {
    console.log(`REMOVE ITEM FROM ARRAY`);

    var updatedItem;

    return new Promise((resolve, reject) => {
        removeValueFromArray(indexName, id, arrayName, value)
            .then(async response => {
                if (response.result === 'updated' || response.result === 'noop' ) {
                    updatedItem = await searchItemById(id);
                    resolve(updatedItem);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Compose item from hit source.
 * 
 * @param {*} hit 
 */
const composeItem = (hit => {
    var item = new Object();

    item.id = hit._id;

    Object.keys(hit._source).forEach(key => {
        if (key !== 'type') {
            item[key] = hit._source[key];
        }
    });

    return item;
});