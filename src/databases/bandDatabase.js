import dotenv from 'dotenv';
import { createDocument, updateDocument, deleteDocument, searchDocument, searchDocumentById } from '../utils/documentUtil';

dotenv.config();

const indexName = process.env.INDEX_NAME;

/**
 * Create band.
 */
export const createBand = band => {
    band.type = 'band';

    return new Promise((resolve, reject) => {
        createDocument(indexName, band)
            .then(async response => {
                if ( response.result === 'created' ) {
                    var createdBand = await searchBandById(response._id);
                    resolve(createdBand);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Search bands.
 */
export const searchBands = () => {
    console.log(`SEARCH BANDS`);
    const query = {
        query: {
            bool: {
                must: {
                    match: {
                        type: 'band'
                    }
                }
            }
        }
    }

    var bands = new Array();
    
    return new Promise((resolve, reject) => {
        searchDocument(indexName, query)
            .then(response => {
                if ( response.hits.total.value > 0) {
                    response.hits.hits.forEach(hit => {
                        bands.push(composeBand(hit));
                    });
                    resolve(bands);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                console.log(`ERROR: ${error}`);
                reject(error);
            });
    });
}

/**
 * Get band by name.
 */
export const searchBandByName= name => {
    const query = {
        query: {
            bool: {
                must: {
                    match: {
                        name: name
                    }
                },
                filter: {
                    term: {
                        type: "band"
                    }
                }
            }
        }
    }

    var band;

    return new Promise((resolve, reject) => {
        searchDocument(indexName, query)
            .then(response => {
                if ( response.hits.total.value > 0) {
                    response.hits.hits.forEach(hit => {
                        if ( hit._score === response.hits.max_score ) {
                            band = composeBand(hit);
                        }
                    });

                    resolve(band);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Get band by ID.
 */
export const searchBandById = id => {
    return new Promise((resolve, reject) => {
        searchDocumentById(indexName, id)
            .then(response => {
                if ( response.found ) {
                    var band = composeBand(response);
                    resolve(band);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Update band.
 */
export const updateBand = (id, band) => {
    return new Promise((resolve, reject) => {
        updateDocument(indexName, id, band)
            .then(async response => {
                if ( response.result === 'updated' || response.result === 'noop' ) {
                    var updatedBand = await searchBandById(id);
                    resolve(updatedBand);
                } else {
                    resolve(undefined);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Delete band.
 */
export const deleteBand = id => {
    return new Promise((resolve, reject) => {
        deleteDocument(indexName, id)
            .then(response => {
                if ( response.result === 'deleted' ) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * Compose band object from hit source.
 * 
 * @param {*} hit 
 */
const composeBand = (hit => {
    var band = new Object();

    band.id = hit._id;
    band.name = hit._source.name;
    band.genre = hit._source.genre;
    band.established = hit._source.established || undefined;
    band.members = hit._source.members || [];
    band.tags = hit._source.tags || [];

    return band;
});