import '@babel/polyfill';
const sinon = require("sinon");
const documentUtil = require('../../utils/documentUtil');
import { searchItemById } from '../itemDatabase';

var sandbox;

let searchReults = {
    "_index": "studiomanager",
    "_type": "_doc",
    "_id": "C0tDfW4BfrRsb4l-McNY",
    "_version": 2,
    "_seq_no": 2,
    "_primary_term": 1,
    "found": true,
    "_source": {
        "name": "Stamina",
        "type": "band",
        "genre": "Suomi-metalli"
    }
}

let serachResultsNoHits = {
    "_index": "studiomanager",
    "_type": "_doc",
    "_id": "C0tDfW4BfrRsb4l-McN",
    "found": false
}

let band = {
    "id": "C0tDfW4BfrRsb4l-McNY",
    "name": "Stamina",
    "genre": "Suomi-metalli"
}

describe("SEARCH BAND", () => {
    
    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it("get one band", async function() {
        sandbox.stub(documentUtil, 'searchDocumentById').resolves(searchReults);
        
        // expect(await searchBandById()).toStrictEqual(band);
        expect(await searchItemById()).toStrictEqual(band);

        sandbox.assert.calledOnce(documentUtil.searchDocumentById);
    });

    it("get no band", async function() {
        sandbox.stub(documentUtil, 'searchDocumentById').resolves(serachResultsNoHits);
        
        // expect(await searchBandById()).toStrictEqual(undefined);
        expect(await searchItemById()).toStrictEqual(undefined);
        
        sandbox.assert.calledOnce(documentUtil.searchDocumentById);
    });

    it("get error", async function() {
        sandbox.stub(documentUtil, 'searchDocumentById').rejects();
        
        try {
            // const result = await searchBandById();
            const result = await searchItemById();
        } catch(error) {
            expect(error).not.toBeNull();
        }

        sandbox.assert.calledOnce(documentUtil.searchDocumentById);
    });
});

