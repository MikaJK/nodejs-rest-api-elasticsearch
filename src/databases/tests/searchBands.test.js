import '@babel/polyfill';
const sinon = require("sinon");
const documentUtil = require('../../utils/documentUtil');
import { searchItems } from '../itemDatabase';

var sandbox;

let searchReults = {
    "took": 4,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 2,
            "relation": "eq"
        },
        "max_score": 0.18232156,
        "hits": [
            {
                "_index": "studiomanager",
                "_type": "_doc",
                "_id": "C0tDfW4BfrRsb4l-McNY",
                "_score": 0.18232156,
                "_source": {
                    "name": "Stamina",
                    "type": "band",
                    "genre": "Suomi-metalli",
                }
            },
            {
                "_index": "studiomanager",
                "_type": "_doc",
                "_id": "DUtQfW4BfrRsb4l-hMN-",
                "_score": 0.18232156,
                "_source": {
                    "name": "Kotiteollisuus",
                    "type": "band",
                    "genre": "Suomi-rock"
                }
            }
        ]
    }
}

let searchResultsNoHits = {
    "took": 4,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 0,
            "relation": "eq"
        },
        "max_score": 0.18232156,
    }
}

let bands = [
    {
        "id": "C0tDfW4BfrRsb4l-McNY",
        "name": "Stamina",
        "genre": "Suomi-metalli"
    },
    {
        "id": "DUtQfW4BfrRsb4l-hMN-",
        "name": "Kotiteollisuus",
        "genre": "Suomi-rock"
    }
]

describe("SEARCH BANDS", () => {
    
    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it("get two bands", async function() {
        sandbox.stub(documentUtil, 'searchDocument').resolves(searchReults);

        // expect(await searchBands()).toStrictEqual(bands);
        expect(await searchItems('band')).toStrictEqual(bands);

        sandbox.assert.calledOnce(documentUtil.searchDocument);
    });

    it("get no bands", async function() {
        sandbox.stub(documentUtil, 'searchDocument').resolves(searchResultsNoHits);

        // expect(await searchBands()).toStrictEqual(undefined);
        expect(await searchItems('band')).toStrictEqual(undefined);

        sandbox.assert.calledOnce(documentUtil.searchDocument);
    });

    it("get error", async function() {
        sandbox.stub(documentUtil, 'searchDocument').rejects();
        
        try {
            // const result = await searchBands();
            const result = await searchItems('band');
        } catch(error) {
            expect(error).not.toBeNull();
        }

        sandbox.assert.calledOnce(documentUtil.searchDocument);
    });
});

