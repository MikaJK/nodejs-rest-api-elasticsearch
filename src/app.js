'use strict';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import bandRouter  from './routes/bandRouter';
import memberRouter from './routes/memberRouter';
var app = express();

dotenv.config();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set("json spaces", 2);

bandRouter(app);
memberRouter(app);

export default app;