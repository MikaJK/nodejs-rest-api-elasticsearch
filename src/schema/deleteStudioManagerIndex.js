const deleteIndex = require('./deleteIndex');

async function deleteStudioManager() {
    try {
        const resp = await deleteIndex('band');
        console.log(resp);
    } catch (e) {
        console.log(e);
    }
}

deleteStudioManager();