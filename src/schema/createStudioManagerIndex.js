const createIndex = require('../utils/createIndex');

async function createStudioManagerIndex() {
    try {
        const resp = await createIndex('studiomanager');
        console.log(resp);
    } catch (e) {
        console.log(e);
    }
}

createStudioManagerIndex();