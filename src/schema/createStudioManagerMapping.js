const createMapping = require('../utils/createMapping');

const indexName = "studiomanager";
const typeName = "member";

async function createStudioManagerMapping() {
    
    const studioManagerMapping = {
        properties: {
            type: {
                type: "keyword"
            },
            band_name: {
                type: "text"
            },
            genre: {
                type: "text"
            },
            established: {
                type: "integer"
            },
            members: {
                type: "text",
            },
            first_name: {
                type: "text"
            },
            last_name: {
                type: "text"
            },
            instruments: {
                type: "text"
            },
            tags: {
                type: "keyword"
            }
        }
    }
    
    try {
        const response = await createMapping(indexName, studioManagerMapping);
        console.log(response);
    } catch (error) {
        console.log(error);
    }
}

createStudioManagerMapping();
