'use strict'
import app from './app';
var port = process.env.PORT || 3000;
app.listen(port);
console.log(`Studio Manager REST service started on port: ${port}`);