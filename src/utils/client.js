const elasticsearch = require('elasticsearch');

exports.client = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'trace'
});