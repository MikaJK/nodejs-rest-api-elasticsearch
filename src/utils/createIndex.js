const client = require('./client');

const createIndex = async (indexName) => {
    return await client.indices.create({
        index: indexName
    });
}

module.exports = createIndex;