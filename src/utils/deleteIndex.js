const client = require('./client');

const deleteIndex = async (indexName) => {
    return await client.indices.delete({
        index: indexName
    });
}

module.exports = deleteIndex;