const client = require('./client');

exports.addValueToArray = async (indexName, id, arrayName, value) => {
    var inline = `ctx._source.${arrayName}.add(params.value)`;
    var params = {
        value: value
    }

    return await client.update({
        index: indexName,
        id: id,
        body: {
            script :{
                inline: inline,
                lang: 'painless',
                params: params
            }
        }
    });
}

// remove value from array.
exports.removeValueFromArray = async(indexName, id, arrayName, value) => {
    var inline = `ctx._source.${arrayName}.remove(params.value)`;
    var params = {
        value: value
    }

    return await client.update({
        index: indexName,
        id: id,
        body: {
            script :{
                inline: inline,
                lang: 'painless',
                params: params
            }
        }
    });
}