//const client = require('./client');
import client from './client';

exports.ping = () => {
    client.ping({
        // ping usually has a 3000ms timeout
        requestTimeout: 1000
    }, (error) => {
        if (error) {
            console.trace('elasticsearch cluster is down!');
        } else {
            console.log('All is well');
        }
    });
}