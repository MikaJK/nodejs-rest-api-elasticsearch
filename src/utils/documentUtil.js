import { client } from './client';

export async function createDocument (indexName, data) {
    return await client.index({
        index: indexName,
        body: data
    });
}

export async function updateDocument(indexName, id, data) {
    return await client.update({
        index: indexName,
        id: id,
        body: {
            doc: data
        }
    });
}

export async function deleteDocument(indexName, id) {
    return new Promise((resolve, reject) => {
        client.delete({
            index: indexName,
            id: id,
            ignore: 404
        })
            .then(response => {
                resolve(response);
            })
            .catch(error => {
                reject(error);
            });
    });
}

export function searchDocument(indexName, query) {
    return new Promise((resolve, reject) => {
        client.search({
            index: indexName,
            body:query
        })
            .then(response => {
                console.log(`UTIL RESPONSE: ${response}`);
                resolve(response);
            })
            .catch(error => {
                reject(error);
            });
    });
}

export async function searchDocumentById(indexName, id) {
    return new Promise((resolve, reject) => {
        client.get({
            index: indexName,
            id: id,
            ignore: 404
        })
            .then(response => {
                resolve(response);
            })
            .catch(error => {
                reject(error);
            });
    });
}

export async function addValueToArray(indexName, id, arrayName, value) {
    var inline = `ctx._source.${arrayName}.add(params.value)`;
    var params = {
        value: value
    }

    return await client.update({
        index: indexName,
        id: id,
        ignore: 404,
        body: {
            script :{
                inline: inline,
                lang: 'painless',
                params: params
            }
        }
    });
}

export async function removeValueFromArray(indexName, id, arrayName, value) {
    //var inline = `if (ctx._source.${arrayName}.contains(params.value)) { ctx._source.${arrayName}.remove(ctx._source.${arrayName}.indexOf(params.value))}`;
    var inline = `ctx._source.${arrayName}.remove(ctx._source.${arrayName}.indexOf(params.value))`;
    var params = {
        value: value
    }

    return await client.update({
        index: indexName,
        id: id,
        ignore: 404,
        body: {
            script :{
                inline: inline,
                lang: 'painless',
                params: params
            }
        }
    });
}