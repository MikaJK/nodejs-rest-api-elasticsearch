const client = require('../utils/client');

const createMapping = async function(indexName, mapping) {
    console.log(mapping);
    return await client.indices.putMapping({
        index: indexName,
        body: mapping
    });
}

module.exports = createMapping;