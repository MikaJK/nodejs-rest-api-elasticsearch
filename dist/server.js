/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/server.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/app.js":
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var cors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! cors */ \"cors\");\n/* harmony import */ var cors__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(cors__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! body-parser */ \"body-parser\");\n/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! dotenv */ \"dotenv\");\n/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(dotenv__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _routes_bandRouter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./routes/bandRouter */ \"./src/routes/bandRouter.js\");\n/* harmony import */ var _routes_memberRouter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./routes/memberRouter */ \"./src/routes/memberRouter.js\");\n\n\n\n\n\n\n\n\nvar app = express__WEBPACK_IMPORTED_MODULE_0___default()();\ndotenv__WEBPACK_IMPORTED_MODULE_3___default.a.config();\napp.use(cors__WEBPACK_IMPORTED_MODULE_1___default()());\napp.use(body_parser__WEBPACK_IMPORTED_MODULE_2___default.a.urlencoded({\n  extended: true\n}));\napp.use(body_parser__WEBPACK_IMPORTED_MODULE_2___default.a.json());\napp.set(\"json spaces\", 2);\nObject(_routes_bandRouter__WEBPACK_IMPORTED_MODULE_4__[\"default\"])(app);\nObject(_routes_memberRouter__WEBPACK_IMPORTED_MODULE_5__[\"default\"])(app);\n/* harmony default export */ __webpack_exports__[\"default\"] = (app);\n\n//# sourceURL=webpack:///./src/app.js?");

/***/ }),

/***/ "./src/controllers/bandController.js":
/*!*******************************************!*\
  !*** ./src/controllers/bandController.js ***!
  \*******************************************/
/*! exports provided: httpPostBand, httpGetBands, httpGetBand, httpPutBand, httpDeleteBand, httpPostBandMember, httpDeleteBandMember */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpPostBand\", function() { return httpPostBand; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpGetBands\", function() { return httpGetBands; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpGetBand\", function() { return httpGetBand; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpPutBand\", function() { return httpPutBand; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpDeleteBand\", function() { return httpDeleteBand; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpPostBandMember\", function() { return httpPostBandMember; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpDeleteBandMember\", function() { return httpDeleteBandMember; });\n/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ \"@babel/runtime/regenerator\");\n/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../databases/itemDatabase */ \"./src/databases/itemDatabase.js\");\n\n\n\n\nvar type = 'band';\n/**\r\n * Post band.\r\n */\n\nvar httpPostBand = function httpPostBand(request, response) {\n  var band, result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpPostBand$(_context) {\n    while (1) {\n      switch (_context.prev = _context.next) {\n        case 0:\n          console.log(\"HTTP POST\");\n          band = request.body;\n          _context.prev = 2;\n          _context.next = 5;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"createItem\"])(band, type));\n\n        case 5:\n          result = _context.sent;\n\n          if (!(result !== undefined)) {\n            _context.next = 10;\n            break;\n          }\n\n          return _context.abrupt(\"return\", response.status(201).send(result));\n\n        case 10:\n          return _context.abrupt(\"return\", response.status(400).send('Bad request; no band returned.'));\n\n        case 11:\n          _context.next = 16;\n          break;\n\n        case 13:\n          _context.prev = 13;\n          _context.t0 = _context[\"catch\"](2);\n          return _context.abrupt(\"return\", response.status(400).send(_context.t0));\n\n        case 16:\n        case \"end\":\n          return _context.stop();\n      }\n    }\n  }, null, null, [[2, 13]]);\n};\n/**\r\n * Get all bands.\r\n */\n\nvar httpGetBands = function httpGetBands(request, response) {\n  var result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpGetBands$(_context2) {\n    while (1) {\n      switch (_context2.prev = _context2.next) {\n        case 0:\n          console.log(\"HTTP GET ALL\");\n          _context2.prev = 1;\n          _context2.next = 4;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"searchItems\"])(type));\n\n        case 4:\n          result = _context2.sent;\n\n          if (!(result !== undefined)) {\n            _context2.next = 9;\n            break;\n          }\n\n          return _context2.abrupt(\"return\", response.status(200).send(result));\n\n        case 9:\n          return _context2.abrupt(\"return\", response.status(404).send('Bands not found'));\n\n        case 10:\n          _context2.next = 15;\n          break;\n\n        case 12:\n          _context2.prev = 12;\n          _context2.t0 = _context2[\"catch\"](1);\n          return _context2.abrupt(\"return\", response.status(400).send(_context2.t0));\n\n        case 15:\n        case \"end\":\n          return _context2.stop();\n      }\n    }\n  }, null, null, [[1, 12]]);\n};\n/**\r\n * Get band.\r\n */\n\nvar httpGetBand = function httpGetBand(request, response) {\n  var id, result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpGetBand$(_context3) {\n    while (1) {\n      switch (_context3.prev = _context3.next) {\n        case 0:\n          console.log(\"HTTP GET\");\n          id = request.params.band_id;\n          _context3.prev = 2;\n          _context3.next = 5;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"searchItemById\"])(id));\n\n        case 5:\n          result = _context3.sent;\n\n          if (!(result !== undefined)) {\n            _context3.next = 10;\n            break;\n          }\n\n          return _context3.abrupt(\"return\", response.status(200).send(result));\n\n        case 10:\n          return _context3.abrupt(\"return\", response.status(404).send('Band not found'));\n\n        case 11:\n          _context3.next = 16;\n          break;\n\n        case 13:\n          _context3.prev = 13;\n          _context3.t0 = _context3[\"catch\"](2);\n          return _context3.abrupt(\"return\", response.status(400).send(_context3.t0));\n\n        case 16:\n        case \"end\":\n          return _context3.stop();\n      }\n    }\n  }, null, null, [[2, 13]]);\n};\n/**\r\n * Update band.\r\n */\n\nvar httpPutBand = function httpPutBand(request, response) {\n  var id, band, result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpPutBand$(_context4) {\n    while (1) {\n      switch (_context4.prev = _context4.next) {\n        case 0:\n          console.log(\"HTTP PUT\");\n          id = request.params.band_id;\n          band = request.body;\n          _context4.prev = 3;\n          _context4.next = 6;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"updateItem\"])(id, band));\n\n        case 6:\n          result = _context4.sent;\n\n          if (!(result !== undefined)) {\n            _context4.next = 11;\n            break;\n          }\n\n          return _context4.abrupt(\"return\", response.status(200).send(result));\n\n        case 11:\n          return _context4.abrupt(\"return\", response.status(404).send('Band not found'));\n\n        case 12:\n          _context4.next = 17;\n          break;\n\n        case 14:\n          _context4.prev = 14;\n          _context4.t0 = _context4[\"catch\"](3);\n          return _context4.abrupt(\"return\", response.status(400).send(_context4.t0));\n\n        case 17:\n        case \"end\":\n          return _context4.stop();\n      }\n    }\n  }, null, null, [[3, 14]]);\n};\n/**\r\n * Delete band.\r\n */\n\nvar httpDeleteBand = function httpDeleteBand(request, response) {\n  var id, result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpDeleteBand$(_context5) {\n    while (1) {\n      switch (_context5.prev = _context5.next) {\n        case 0:\n          console.log(\"HTTP DELETE\");\n          id = request.params.band_id;\n          _context5.prev = 2;\n          _context5.next = 5;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"deleteItem\"])(id));\n\n        case 5:\n          result = _context5.sent;\n\n          if (!result) {\n            _context5.next = 10;\n            break;\n          }\n\n          return _context5.abrupt(\"return\", response.status(204).send(result));\n\n        case 10:\n          return _context5.abrupt(\"return\", response.status(404).send('Band not found'));\n\n        case 11:\n          _context5.next = 16;\n          break;\n\n        case 13:\n          _context5.prev = 13;\n          _context5.t0 = _context5[\"catch\"](2);\n          return _context5.abrupt(\"return\", response.status(400).send(_context5.t0));\n\n        case 16:\n        case \"end\":\n          return _context5.stop();\n      }\n    }\n  }, null, null, [[2, 13]]);\n};\nvar httpPostBandMember = function httpPostBandMember(request, response) {\n  var id, member, result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpPostBandMember$(_context6) {\n    while (1) {\n      switch (_context6.prev = _context6.next) {\n        case 0:\n          console.log(\"HTTP POST BAND MEMBER\");\n          id = request.params.band_id;\n          member = request.body;\n          _context6.prev = 3;\n          _context6.next = 6;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"addToItemArray\"])(id, 'members', member.id));\n\n        case 6:\n          result = _context6.sent;\n\n          if (!result) {\n            _context6.next = 11;\n            break;\n          }\n\n          return _context6.abrupt(\"return\", response.status(201).send(result));\n\n        case 11:\n          return _context6.abrupt(\"return\", response.status(404).send('Band not found'));\n\n        case 12:\n          _context6.next = 17;\n          break;\n\n        case 14:\n          _context6.prev = 14;\n          _context6.t0 = _context6[\"catch\"](3);\n          return _context6.abrupt(\"return\", response.status(400).send(_context6.t0));\n\n        case 17:\n        case \"end\":\n          return _context6.stop();\n      }\n    }\n  }, null, null, [[3, 14]]);\n};\nvar httpDeleteBandMember = function httpDeleteBandMember(request, response) {\n  var band_id, member_id, result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpDeleteBandMember$(_context7) {\n    while (1) {\n      switch (_context7.prev = _context7.next) {\n        case 0:\n          console.log(\"HTTP DELETE BAND MEMBER\");\n          band_id = request.params.band_id;\n          member_id = request.params.member_id;\n          _context7.prev = 3;\n          _context7.next = 6;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"removeFromItemArray\"])(band_id, 'members', member_id));\n\n        case 6:\n          result = _context7.sent;\n\n          if (!result) {\n            _context7.next = 12;\n            break;\n          }\n\n          console.log(\"RESULT: \".concat(JSON.stringify(result)));\n          return _context7.abrupt(\"return\", response.status(200).send(result));\n\n        case 12:\n          return _context7.abrupt(\"return\", response.status(404).send('Band not found'));\n\n        case 13:\n          _context7.next = 18;\n          break;\n\n        case 15:\n          _context7.prev = 15;\n          _context7.t0 = _context7[\"catch\"](3);\n          return _context7.abrupt(\"return\", response.status(400).send(_context7.t0));\n\n        case 18:\n        case \"end\":\n          return _context7.stop();\n      }\n    }\n  }, null, null, [[3, 15]]);\n};\n\n//# sourceURL=webpack:///./src/controllers/bandController.js?");

/***/ }),

/***/ "./src/controllers/memberController.js":
/*!*********************************************!*\
  !*** ./src/controllers/memberController.js ***!
  \*********************************************/
/*! exports provided: httpPostMember, httpGetMembers, httpGetMember, httpPutMember, httpDeleteMember */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpPostMember\", function() { return httpPostMember; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpGetMembers\", function() { return httpGetMembers; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpGetMember\", function() { return httpGetMember; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpPutMember\", function() { return httpPutMember; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"httpDeleteMember\", function() { return httpDeleteMember; });\n/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ \"@babel/runtime/regenerator\");\n/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../databases/itemDatabase */ \"./src/databases/itemDatabase.js\");\n\n\n\n\nvar type = 'member';\n/**\r\n * Post member.\r\n */\n\nvar httpPostMember = function httpPostMember(request, response) {\n  var member, result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpPostMember$(_context) {\n    while (1) {\n      switch (_context.prev = _context.next) {\n        case 0:\n          console.log(\"HTTP POST MEMBER\");\n          member = request.body;\n          _context.prev = 2;\n          _context.next = 5;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"createItem\"])(member, type));\n\n        case 5:\n          result = _context.sent;\n\n          if (!(result !== undefined)) {\n            _context.next = 10;\n            break;\n          }\n\n          return _context.abrupt(\"return\", response.status(201).send(result));\n\n        case 10:\n          return _context.abrupt(\"return\", response.status(400).send('Bad request; no member returned.'));\n\n        case 11:\n          _context.next = 16;\n          break;\n\n        case 13:\n          _context.prev = 13;\n          _context.t0 = _context[\"catch\"](2);\n          return _context.abrupt(\"return\", response.status(400).send(_context.t0));\n\n        case 16:\n        case \"end\":\n          return _context.stop();\n      }\n    }\n  }, null, null, [[2, 13]]);\n};\n/**\r\n * Get all members.\r\n */\n\nvar httpGetMembers = function httpGetMembers(request, response) {\n  var result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpGetMembers$(_context2) {\n    while (1) {\n      switch (_context2.prev = _context2.next) {\n        case 0:\n          console.log(\"HTTP GET ALL MEMBERS\");\n          _context2.prev = 1;\n          _context2.next = 4;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"searchItems\"])(type));\n\n        case 4:\n          result = _context2.sent;\n\n          if (!(result !== undefined)) {\n            _context2.next = 9;\n            break;\n          }\n\n          return _context2.abrupt(\"return\", response.status(200).send(result));\n\n        case 9:\n          return _context2.abrupt(\"return\", response.status(404).send('Members not found'));\n\n        case 10:\n          _context2.next = 15;\n          break;\n\n        case 12:\n          _context2.prev = 12;\n          _context2.t0 = _context2[\"catch\"](1);\n          return _context2.abrupt(\"return\", response.status(400).send(_context2.t0));\n\n        case 15:\n        case \"end\":\n          return _context2.stop();\n      }\n    }\n  }, null, null, [[1, 12]]);\n};\n/**\r\n * Get member.\r\n */\n\nvar httpGetMember = function httpGetMember(request, response) {\n  var id, result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpGetMember$(_context3) {\n    while (1) {\n      switch (_context3.prev = _context3.next) {\n        case 0:\n          console.log(\"HTTP GET MEMBER\");\n          id = request.params.member_id;\n          console.log(id);\n          _context3.prev = 3;\n          _context3.next = 6;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"searchItemById\"])(id));\n\n        case 6:\n          result = _context3.sent;\n\n          if (!(result !== undefined)) {\n            _context3.next = 11;\n            break;\n          }\n\n          return _context3.abrupt(\"return\", response.status(200).send(result));\n\n        case 11:\n          return _context3.abrupt(\"return\", response.status(404).send('Member not found'));\n\n        case 12:\n          _context3.next = 17;\n          break;\n\n        case 14:\n          _context3.prev = 14;\n          _context3.t0 = _context3[\"catch\"](3);\n          return _context3.abrupt(\"return\", response.status(400).send(_context3.t0));\n\n        case 17:\n        case \"end\":\n          return _context3.stop();\n      }\n    }\n  }, null, null, [[3, 14]]);\n};\n/**\r\n * Update member.\r\n */\n\nvar httpPutMember = function httpPutMember(request, response) {\n  var id, member, result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpPutMember$(_context4) {\n    while (1) {\n      switch (_context4.prev = _context4.next) {\n        case 0:\n          console.log(\"HTTP PUT MEMBER\");\n          id = request.params.member_id;\n          member = request.body;\n          _context4.prev = 3;\n          _context4.next = 6;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"updateItem\"])(id, member));\n\n        case 6:\n          result = _context4.sent;\n\n          if (!(result !== undefined)) {\n            _context4.next = 11;\n            break;\n          }\n\n          return _context4.abrupt(\"return\", response.status(200).send(result));\n\n        case 11:\n          return _context4.abrupt(\"return\", response.status(404).send('Member not found'));\n\n        case 12:\n          _context4.next = 17;\n          break;\n\n        case 14:\n          _context4.prev = 14;\n          _context4.t0 = _context4[\"catch\"](3);\n          return _context4.abrupt(\"return\", response.status(400).send(_context4.t0));\n\n        case 17:\n        case \"end\":\n          return _context4.stop();\n      }\n    }\n  }, null, null, [[3, 14]]);\n};\n/**\r\n * Delete member.\r\n */\n\nvar httpDeleteMember = function httpDeleteMember(request, response) {\n  var id, result;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function httpDeleteMember$(_context5) {\n    while (1) {\n      switch (_context5.prev = _context5.next) {\n        case 0:\n          console.log(\"HTTP DELETE MEMBER\");\n          id = request.params.member_id;\n          _context5.prev = 2;\n          _context5.next = 5;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(Object(_databases_itemDatabase__WEBPACK_IMPORTED_MODULE_1__[\"deleteItem\"])(id));\n\n        case 5:\n          result = _context5.sent;\n\n          if (!result) {\n            _context5.next = 10;\n            break;\n          }\n\n          return _context5.abrupt(\"return\", response.status(204).send(result));\n\n        case 10:\n          return _context5.abrupt(\"return\", response.status(404).send('Member not found'));\n\n        case 11:\n          _context5.next = 16;\n          break;\n\n        case 13:\n          _context5.prev = 13;\n          _context5.t0 = _context5[\"catch\"](2);\n          return _context5.abrupt(\"return\", response.status(400).send(_context5.t0));\n\n        case 16:\n        case \"end\":\n          return _context5.stop();\n      }\n    }\n  }, null, null, [[2, 13]]);\n};\n\n//# sourceURL=webpack:///./src/controllers/memberController.js?");

/***/ }),

/***/ "./src/databases/itemDatabase.js":
/*!***************************************!*\
  !*** ./src/databases/itemDatabase.js ***!
  \***************************************/
/*! exports provided: createItem, searchItems, searchItemByKey, searchItemById, updateItem, deleteItem, addToItemArray, removeFromItemArray */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"createItem\", function() { return createItem; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"searchItems\", function() { return searchItems; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"searchItemByKey\", function() { return searchItemByKey; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"searchItemById\", function() { return searchItemById; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"updateItem\", function() { return updateItem; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"deleteItem\", function() { return deleteItem; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addToItemArray\", function() { return addToItemArray; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"removeFromItemArray\", function() { return removeFromItemArray; });\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"@babel/runtime/helpers/defineProperty\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/regenerator */ \"@babel/runtime/regenerator\");\n/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dotenv */ \"dotenv\");\n/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(dotenv__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _utils_documentUtil__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/documentUtil */ \"./src/utils/documentUtil.js\");\n\n\n\n\ndotenv__WEBPACK_IMPORTED_MODULE_2___default.a.config();\nvar indexName = process.env.INDEX_NAME;\n/**\r\n * Create item.\r\n */\n\nvar createItem = function createItem(item, type) {\n  console.log(\"CREATE ITEM\");\n  item.type = type;\n  return new Promise(function (resolve, reject) {\n    Object(_utils_documentUtil__WEBPACK_IMPORTED_MODULE_3__[\"createDocument\"])(indexName, item).then(function _callee(response) {\n      var createdItem;\n      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function _callee$(_context) {\n        while (1) {\n          switch (_context.prev = _context.next) {\n            case 0:\n              if (!(response.result === 'created')) {\n                _context.next = 7;\n                break;\n              }\n\n              _context.next = 3;\n              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(searchItemById(response._id));\n\n            case 3:\n              createdItem = _context.sent;\n              resolve(createdItem);\n              _context.next = 8;\n              break;\n\n            case 7:\n              resolve(undefined);\n\n            case 8:\n            case \"end\":\n              return _context.stop();\n          }\n        }\n      });\n    })[\"catch\"](function (error) {\n      reject(error);\n    });\n  });\n};\n/**\r\n * Search items.\r\n */\n\nvar searchItems = function searchItems(type) {\n  console.log(\"SEARCH ITEMS\");\n  var query = {\n    query: {\n      bool: {\n        must: {\n          match: {\n            type: type\n          }\n        }\n      }\n    }\n  };\n  var items = new Array();\n  return new Promise(function (resolve, reject) {\n    Object(_utils_documentUtil__WEBPACK_IMPORTED_MODULE_3__[\"searchDocument\"])(indexName, query).then(function (response) {\n      if (response.hits.total.value > 0) {\n        response.hits.hits.forEach(function (hit) {\n          items.push(composeItem(hit));\n        });\n        resolve(items);\n      } else {\n        resolve(undefined);\n      }\n    })[\"catch\"](function (error) {\n      reject(error);\n    });\n  });\n};\n/**\r\n * Search item by key.\r\n */\n\nvar searchItemByKey = function searchItemByKey(key, value, type) {\n  console.log(\"SEARCH ITEM BY KEY\");\n  var query = {\n    query: {\n      bool: {\n        must: {\n          match: _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, key, value)\n        },\n        filter: {\n          term: {\n            type: type\n          }\n        }\n      }\n    }\n  };\n  var item;\n  return new Promise(function (resolve, reject) {\n    Object(_utils_documentUtil__WEBPACK_IMPORTED_MODULE_3__[\"searchDocument\"])(indexName, query).then(function (response) {\n      if (response.hits.total.value > 0) {\n        response.hits.hits.forEach(function (hit) {\n          if (hit._score === response.hits.max_score) {\n            item = composeBand(hit);\n          }\n        });\n        resolve(item);\n      } else {\n        resolve(undefined);\n      }\n    })[\"catch\"](function (error) {\n      reject(error);\n    });\n  });\n};\n/**\r\n * Search item by ID.\r\n */\n\nvar searchItemById = function searchItemById(id) {\n  console.log(\"SEARCH ITEM BY ID\");\n  var item;\n  return new Promise(function (resolve, reject) {\n    Object(_utils_documentUtil__WEBPACK_IMPORTED_MODULE_3__[\"searchDocumentById\"])(indexName, id).then(function (response) {\n      if (response.found) {\n        item = composeItem(response);\n        resolve(item);\n      } else {\n        resolve(undefined);\n      }\n    })[\"catch\"](function (error) {\n      reject(error);\n    });\n  });\n};\n/**\r\n * Update item.\r\n */\n\nvar updateItem = function updateItem(id, item) {\n  console.log(\"UPDATE ITEM\");\n  var updatedItem;\n  return new Promise(function (resolve, reject) {\n    Object(_utils_documentUtil__WEBPACK_IMPORTED_MODULE_3__[\"updateDocument\"])(indexName, id, item).then(function _callee2(response) {\n      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function _callee2$(_context2) {\n        while (1) {\n          switch (_context2.prev = _context2.next) {\n            case 0:\n              if (!(response.result === 'updated' || response.result === 'noop')) {\n                _context2.next = 7;\n                break;\n              }\n\n              _context2.next = 3;\n              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(searchItemById(id));\n\n            case 3:\n              updatedItem = _context2.sent;\n              resolve(updatedItem);\n              _context2.next = 8;\n              break;\n\n            case 7:\n              resolve(undefined);\n\n            case 8:\n            case \"end\":\n              return _context2.stop();\n          }\n        }\n      });\n    })[\"catch\"](function (error) {\n      reject(error);\n    });\n  });\n};\n/**\r\n * Delete item.\r\n */\n\nvar deleteItem = function deleteItem(id) {\n  console.log(\"DELETE ITEM\");\n  return new Promise(function (resolve, reject) {\n    Object(_utils_documentUtil__WEBPACK_IMPORTED_MODULE_3__[\"deleteDocument\"])(indexName, id).then(function (response) {\n      if (response.result === 'deleted') {\n        resolve(true);\n      } else {\n        resolve(false);\n      }\n    })[\"catch\"](function (error) {\n      reject(error);\n    });\n  });\n};\nvar addToItemArray = function addToItemArray(id, arrayName, value) {\n  console.log(\"ADD ITEM TO ARRAY\");\n  var updatedItem;\n  return new Promise(function (resolve, reject) {\n    Object(_utils_documentUtil__WEBPACK_IMPORTED_MODULE_3__[\"addValueToArray\"])(indexName, id, arrayName, value).then(function _callee3(response) {\n      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function _callee3$(_context3) {\n        while (1) {\n          switch (_context3.prev = _context3.next) {\n            case 0:\n              if (!(response.result === 'updated' || response.result === 'noop')) {\n                _context3.next = 8;\n                break;\n              }\n\n              _context3.next = 3;\n              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(searchItemById(id));\n\n            case 3:\n              updatedItem = _context3.sent;\n              console.log(\"THEN OK\");\n              resolve(updatedItem);\n              _context3.next = 10;\n              break;\n\n            case 8:\n              console.log(\"THEN NOK\");\n              resolve(undefined);\n\n            case 10:\n            case \"end\":\n              return _context3.stop();\n          }\n        }\n      });\n    })[\"catch\"](function (error) {\n      console.log(\"CATCH\");\n      reject(error);\n    });\n  });\n};\nvar removeFromItemArray = function removeFromItemArray(id, arrayName, value) {\n  console.log(\"REMOVE ITEM FROM ARRAY\");\n  var updatedItem;\n  return new Promise(function (resolve, reject) {\n    Object(_utils_documentUtil__WEBPACK_IMPORTED_MODULE_3__[\"removeValueFromArray\"])(indexName, id, arrayName, value).then(function _callee4(response) {\n      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function _callee4$(_context4) {\n        while (1) {\n          switch (_context4.prev = _context4.next) {\n            case 0:\n              if (!(response.result === 'updated' || response.result === 'noop')) {\n                _context4.next = 8;\n                break;\n              }\n\n              _context4.next = 3;\n              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(searchItemById(id));\n\n            case 3:\n              updatedItem = _context4.sent;\n              console.log(\"UPDATED BAND: \".concat(JSON.stringify(updatedItem)));\n              resolve(updatedItem);\n              _context4.next = 9;\n              break;\n\n            case 8:\n              resolve(undefined);\n\n            case 9:\n            case \"end\":\n              return _context4.stop();\n          }\n        }\n      });\n    })[\"catch\"](function (error) {\n      reject(error);\n    });\n  });\n};\n/**\r\n * Compose item from hit source.\r\n * \r\n * @param {*} hit \r\n */\n\nvar composeItem = function composeItem(hit) {\n  var item = new Object();\n  item.id = hit._id;\n  Object.keys(hit._source).forEach(function (key) {\n    if (key !== 'type') {\n      item[key] = hit._source[key];\n    }\n  });\n  return item;\n};\n\n//# sourceURL=webpack:///./src/databases/itemDatabase.js?");

/***/ }),

/***/ "./src/routes/bandRouter.js":
/*!**********************************!*\
  !*** ./src/routes/bandRouter.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return bandRouter; });\n\n\nvar band = __webpack_require__(/*! ../controllers/bandController */ \"./src/controllers/bandController.js\");\n\nfunction bandRouter(app) {\n  app.route('/bands').get(band.httpGetBands).post(band.httpPostBand);\n  app.route('/bands/:band_id').get(band.httpGetBand).put(band.httpPutBand)[\"delete\"](band.httpDeleteBand);\n  app.route('/bands/:band_id/members').post(band.httpPostBandMember);\n  app.route('/bands/:band_id/members/:member_id')[\"delete\"](band.httpDeleteBandMember); // app.route('/bands/:band_id/tags')\n  //     .post(band.postTag);\n}\n;\n\n//# sourceURL=webpack:///./src/routes/bandRouter.js?");

/***/ }),

/***/ "./src/routes/memberRouter.js":
/*!************************************!*\
  !*** ./src/routes/memberRouter.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return memberRouter; });\n\n\nvar member = __webpack_require__(/*! ../controllers/memberController */ \"./src/controllers/memberController.js\");\n\nfunction memberRouter(app) {\n  app.route('/members').get(member.httpGetMembers).post(member.httpPostMember);\n  app.route('/members/:member_id').get(member.httpGetMember).put(member.httpPutMember)[\"delete\"](member.httpDeleteMember);\n}\n;\n\n//# sourceURL=webpack:///./src/routes/memberRouter.js?");

/***/ }),

/***/ "./src/server.js":
/*!***********************!*\
  !*** ./src/server.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app */ \"./src/app.js\");\n\n\n\nvar port = process.env.PORT || 3000;\n_app__WEBPACK_IMPORTED_MODULE_0__[\"default\"].listen(port);\nconsole.log(\"Studio Manager REST service started on port: \".concat(port));\n\n//# sourceURL=webpack:///./src/server.js?");

/***/ }),

/***/ "./src/utils/client.js":
/*!*****************************!*\
  !*** ./src/utils/client.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var elasticsearch = __webpack_require__(/*! elasticsearch */ \"elasticsearch\");\n\nexports.client = new elasticsearch.Client({\n  host: 'localhost:9200',\n  log: 'trace'\n});\n\n//# sourceURL=webpack:///./src/utils/client.js?");

/***/ }),

/***/ "./src/utils/documentUtil.js":
/*!***********************************!*\
  !*** ./src/utils/documentUtil.js ***!
  \***********************************/
/*! exports provided: createDocument, updateDocument, deleteDocument, searchDocument, searchDocumentById, addValueToArray, removeValueFromArray */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"createDocument\", function() { return createDocument; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"updateDocument\", function() { return updateDocument; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"deleteDocument\", function() { return deleteDocument; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"searchDocument\", function() { return searchDocument; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"searchDocumentById\", function() { return searchDocumentById; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addValueToArray\", function() { return addValueToArray; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"removeValueFromArray\", function() { return removeValueFromArray; });\n/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ \"@babel/runtime/regenerator\");\n/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _client__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./client */ \"./src/utils/client.js\");\n/* harmony import */ var _client__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_client__WEBPACK_IMPORTED_MODULE_1__);\n\n\nfunction createDocument(indexName, data) {\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function createDocument$(_context) {\n    while (1) {\n      switch (_context.prev = _context.next) {\n        case 0:\n          _context.next = 2;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(_client__WEBPACK_IMPORTED_MODULE_1__[\"client\"].index({\n            index: indexName,\n            body: data\n          }));\n\n        case 2:\n          return _context.abrupt(\"return\", _context.sent);\n\n        case 3:\n        case \"end\":\n          return _context.stop();\n      }\n    }\n  });\n}\nfunction updateDocument(indexName, id, data) {\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function updateDocument$(_context2) {\n    while (1) {\n      switch (_context2.prev = _context2.next) {\n        case 0:\n          _context2.next = 2;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(_client__WEBPACK_IMPORTED_MODULE_1__[\"client\"].update({\n            index: indexName,\n            id: id,\n            body: {\n              doc: data\n            }\n          }));\n\n        case 2:\n          return _context2.abrupt(\"return\", _context2.sent);\n\n        case 3:\n        case \"end\":\n          return _context2.stop();\n      }\n    }\n  });\n}\nfunction deleteDocument(indexName, id) {\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function deleteDocument$(_context3) {\n    while (1) {\n      switch (_context3.prev = _context3.next) {\n        case 0:\n          return _context3.abrupt(\"return\", new Promise(function (resolve, reject) {\n            _client__WEBPACK_IMPORTED_MODULE_1__[\"client\"][\"delete\"]({\n              index: indexName,\n              id: id,\n              ignore: 404\n            }).then(function (response) {\n              resolve(response);\n            })[\"catch\"](function (error) {\n              reject(error);\n            });\n          }));\n\n        case 1:\n        case \"end\":\n          return _context3.stop();\n      }\n    }\n  });\n}\nfunction searchDocument(indexName, query) {\n  return new Promise(function (resolve, reject) {\n    _client__WEBPACK_IMPORTED_MODULE_1__[\"client\"].search({\n      index: indexName,\n      body: query\n    }).then(function (response) {\n      console.log(\"UTIL RESPONSE: \".concat(response));\n      resolve(response);\n    })[\"catch\"](function (error) {\n      reject(error);\n    });\n  });\n}\nfunction searchDocumentById(indexName, id) {\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function searchDocumentById$(_context4) {\n    while (1) {\n      switch (_context4.prev = _context4.next) {\n        case 0:\n          return _context4.abrupt(\"return\", new Promise(function (resolve, reject) {\n            _client__WEBPACK_IMPORTED_MODULE_1__[\"client\"].get({\n              index: indexName,\n              id: id,\n              ignore: 404\n            }).then(function (response) {\n              resolve(response);\n            })[\"catch\"](function (error) {\n              reject(error);\n            });\n          }));\n\n        case 1:\n        case \"end\":\n          return _context4.stop();\n      }\n    }\n  });\n}\nfunction addValueToArray(indexName, id, arrayName, value) {\n  var inline, params;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function addValueToArray$(_context5) {\n    while (1) {\n      switch (_context5.prev = _context5.next) {\n        case 0:\n          inline = \"ctx._source.\".concat(arrayName, \".add(params.value)\");\n          params = {\n            value: value\n          };\n          _context5.next = 4;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(_client__WEBPACK_IMPORTED_MODULE_1__[\"client\"].update({\n            index: indexName,\n            id: id,\n            ignore: 404,\n            body: {\n              script: {\n                inline: inline,\n                lang: 'painless',\n                params: params\n              }\n            }\n          }));\n\n        case 4:\n          return _context5.abrupt(\"return\", _context5.sent);\n\n        case 5:\n        case \"end\":\n          return _context5.stop();\n      }\n    }\n  });\n}\nfunction removeValueFromArray(indexName, id, arrayName, value) {\n  var inline, params;\n  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function removeValueFromArray$(_context6) {\n    while (1) {\n      switch (_context6.prev = _context6.next) {\n        case 0:\n          //var inline = `if (ctx._source.${arrayName}.contains(params.value)) { ctx._source.${arrayName}.remove(ctx._source.${arrayName}.indexOf(params.value))}`;\n          inline = \"ctx._source.\".concat(arrayName, \".remove(ctx._source.\").concat(arrayName, \".indexOf(params.value))\");\n          params = {\n            value: value\n          };\n          _context6.next = 4;\n          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(_client__WEBPACK_IMPORTED_MODULE_1__[\"client\"].update({\n            index: indexName,\n            id: id,\n            body: {\n              script: {\n                inline: inline,\n                lang: 'painless',\n                params: params\n              }\n            }\n          }));\n\n        case 4:\n          return _context6.abrupt(\"return\", _context6.sent);\n\n        case 5:\n        case \"end\":\n          return _context6.stop();\n      }\n    }\n  });\n}\n\n//# sourceURL=webpack:///./src/utils/documentUtil.js?");

/***/ }),

/***/ "@babel/runtime/helpers/defineProperty":
/*!********************************************************!*\
  !*** external "@babel/runtime/helpers/defineProperty" ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/defineProperty\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/defineProperty%22?");

/***/ }),

/***/ "@babel/runtime/regenerator":
/*!*********************************************!*\
  !*** external "@babel/runtime/regenerator" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/regenerator\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/regenerator%22?");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"body-parser\");\n\n//# sourceURL=webpack:///external_%22body-parser%22?");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"cors\");\n\n//# sourceURL=webpack:///external_%22cors%22?");

/***/ }),

/***/ "dotenv":
/*!*************************!*\
  !*** external "dotenv" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"dotenv\");\n\n//# sourceURL=webpack:///external_%22dotenv%22?");

/***/ }),

/***/ "elasticsearch":
/*!********************************!*\
  !*** external "elasticsearch" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"elasticsearch\");\n\n//# sourceURL=webpack:///external_%22elasticsearch%22?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ })

/******/ });